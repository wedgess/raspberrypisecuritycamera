package com.example.gar.pisecuritycamera.home;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.gar.pisecuritycamera.R;
import com.example.gar.pisecuritycamera.model.ImageCapture;

import durdinapps.rxfirebase2.RxFirebaseRecyclerAdapter;

/**
 * Adapter class for the list of image captures displayed in {@link HomeActivity}
 */
public class ImageCaptureAdapter extends RxFirebaseRecyclerAdapter<ImageCaptureAdapter.ViewHolder, ImageCapture> {

    private static final String TAG = ImageCaptureAdapter.class.getSimpleName();

    // click callback for activity
    interface RecyclerClickListener {
        void onRecyclerItemClick(int position);

        void onDeleteItemClick(int position);
    }

    private RecyclerClickListener mRecyclerClickListener;

    ImageCaptureAdapter(RecyclerClickListener recyclerClickListener) {
        super(ImageCapture.class);
        setHasStableIds(true);
        this.mRecyclerClickListener = recyclerClickListener;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public ImageCaptureAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Initialize your Holder
        return new ImageCaptureAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_image, null), mRecyclerClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ImageCapture imageCapture = getItems().get(position);
        // bind image data
        holder.setData(imageCapture);
    }

    @Override
    protected void itemAdded(ImageCapture item, String key, int position) {
        // Add captures id
        item.setId(key);
    }

    @Override
    protected void itemChanged(ImageCapture oldItem, ImageCapture newItem, String key, int position) {
        // Add captures id
        newItem.setId(key);
    }

    @Override
    protected void itemRemoved(ImageCapture item, String key, int position) {
        Log.d(TAG, "Removed an item.");
    }

    @Override
    protected void itemMoved(ImageCapture item, String key, int oldPosition, int newPosition) {
        Log.d(TAG, "Moved an item.");
    }

    // inner class to hold a reference to each item of RecyclerView
    static class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView ivCaptureImg;
        final ImageButton ibCaptureDelete;
        final TextView tvCaptureTitle;
        final TextView tvCaptureTime;

        ViewHolder(View itemLayoutView, final RecyclerClickListener listener) {
            super(itemLayoutView);
            ivCaptureImg = itemLayoutView.findViewById(R.id.iv_capture_img);
            tvCaptureTitle = itemLayoutView.findViewById(R.id.tv_img_title);
            tvCaptureTime = itemLayoutView.findViewById(R.id.tv_img_time);
            ibCaptureDelete = itemLayoutView.findViewById(R.id.ib_delete);
            // send result back to view to delete item
            ibCaptureDelete.setOnClickListener(v -> listener.onDeleteItemClick(getAdapterPosition()));
            // Send result back to activity to launch image activity
            itemLayoutView.setOnClickListener(v -> listener.onRecyclerItemClick(getAdapterPosition()));
        }

        public void setData(ImageCapture data) {
            // set the data to views
            tvCaptureTitle.setText(data.getTitle());
            tvCaptureTime.setText(data.getTimestamp());
            Glide.with(ivCaptureImg.getContext())
                    .load(data.getUrl())
                    .into(ivCaptureImg);

        }
    }
}
