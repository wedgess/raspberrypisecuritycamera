package com.example.gar.pisecuritycamera;

/**
 * Base presenter which all applications presenter classes implement
 */
public interface BasePresenter {

    void subscribe();

    void unSubscribe();

}
