package com.example.gar.pisecuritycamera.home;

import android.support.annotation.NonNull;

import com.example.gar.pisecuritycamera.BasePresenter;
import com.example.gar.pisecuritycamera.BaseView;
import com.example.gar.pisecuritycamera.model.ImageCapture;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

import durdinapps.rxfirebase2.RxFirebaseChildEvent;

/**
 * Contract between {@link HomeActivity} and {@link HomePresenter}
 */
interface HomeContract {

    interface View extends BaseView<HomeContract.Presenter> {

        void updateUi(FirebaseUser user);

        void showLoginFailed(@NonNull String error);

        void toggleLoadingUi(boolean show);

        void manageImageCaptures(RxFirebaseChildEvent<DataSnapshot> captures);

        void setTemperature(@NonNull String temperature);

        void setHumidity(@NonNull String humidity);

        void setPiIPAddress(@NonNull String piIP);

        void setBuzzerState(boolean enabled);

        void setPiStatus(boolean online);


    }

    interface Presenter extends BasePresenter {

        void updateFirebaseDbUser(@NonNull FirebaseUser currentUser, @NonNull String regToken);

        void signIn(@NonNull String email, @NonNull String password);

        void addImageCaptureListener(@NonNull FirebaseUser user);

        void addSensorDataListener(@NonNull FirebaseUser user);

        void deleteImageCapture(@NonNull ImageCapture capture);

        void addPiIPListener(@NonNull FirebaseUser user);

        void addPiStatusListener(@NonNull FirebaseUser user);

        void addBuzzerListener(@NonNull FirebaseUser user);

        void enableBuzzer(boolean enabled);

        void setPiStatusBasedOnConnection(@NonNull FirebaseUser user, boolean hasInternet);
    }
}
