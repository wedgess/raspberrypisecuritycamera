package com.example.gar.pisecuritycamera.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.gar.pisecuritycamera.R;
import com.example.gar.pisecuritycamera.StreamActivity;
import com.example.gar.pisecuritycamera.helpers.NetworkHelper;
import com.example.gar.pisecuritycamera.helpers.PrefHelper;
import com.example.gar.pisecuritycamera.image.ImageViewerActivity;
import com.example.gar.pisecuritycamera.model.ImageCapture;
import com.example.gar.pisecuritycamera.register.RegisterActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import durdinapps.rxfirebase2.RxFirebaseChildEvent;
import io.reactivex.Observable;

public class HomeActivity extends AppCompatActivity implements HomeContract.View, View.OnClickListener, ImageCaptureAdapter.RecyclerClickListener {

    private static final String TAG = HomeActivity.class.getSimpleName();

    public static final int ACTIVITY_RESULT_REGISTER = 101;
    public static final int REQUEST_CODE_EXTERNAL_PERMISSIONS = 102;

    //region View binding
    @BindView(R.id.tv_login_title)
    TextView mLoginTitleTV;
    @BindView(R.id.frame_or_divider)
    FrameLayout mOrDivider;
    @BindView(R.id.til_login_email)
    TextInputLayout mLoginEmailTIL;
    @BindView(R.id.til_login_password)
    TextInputLayout mLoginPasswordTIL;
    @BindView(R.id.tv_pi_ip_value)
    TextView mPiIPValueTV;
    @BindView(R.id.btn_login)
    Button mLoginBTN;
    @BindView(R.id.btn_stream)
    Button mStreamBTN;
    @BindView(R.id.btn_register)
    Button mRegisterBTN;
    @BindView(R.id.rv_captured_images)
    RecyclerView mCapturedImagesRV;
    @BindView(R.id.tv_images_title)
    TextView mCapturedImagesTitleTV;
    @BindView(R.id.card_login)
    CardView mLoginCV;
    @BindView(R.id.card_pi_info)
    CardView mPieInfoCV;
    @BindView(R.id.card_captures)
    CardView mCapturesCard;
    @BindView(R.id.sensor_data_layout)
    LinearLayout mSensorDataLL;
    @BindView(R.id.pb_progess_signin)
    ProgressBar mLoginPB;
    @BindView(R.id.tv_progess_signin)
    TextView mLoginProgressTV;
    @BindView(R.id.tv_empty_images)
    TextView mEmptyImagesTV;
    @BindView(R.id.tv_temperature_value)
    TextView mTemperatureValueTV;
    @BindView(R.id.tv_humidity_value)
    TextView mHumidityValueTV;
    @BindView(R.id.switch_buzzer)
    Switch mBuzzerSwitch;
    //endregion

    private HomeContract.Presenter mPresenter;
    private ImageCaptureAdapter mAdapter;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set-up view, view binding, activity actionbar, presenter and listeners
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // set Firebase Database offline
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        new HomePresenter(this);
        mLoginBTN.setOnClickListener(this);
        mRegisterBTN.setOnClickListener(this);
        mStreamBTN.setOnClickListener(this);
        mCapturedImagesRV.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, true));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mCapturedImagesRV.getContext(),
                LinearLayoutManager.VERTICAL);
        mCapturedImagesRV.addItemDecoration(dividerItemDecoration);

        setUpInputObservables();

        // check permissions or request them - for Android Marshmallow or above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_EXTERNAL_PERMISSIONS);
            }
        }

        mBuzzerSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.enableBuzzer(isChecked));

        // subscribe to presenter to set up UI
        mPresenter.subscribe();

        registerReceiver(mNetworkBroadcastReceiver,
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
            if (currentUser != null) {
                // make sure we are connected to mobile or wifi network and set status
                mPresenter.setPiStatusBasedOnConnection(currentUser, NetworkHelper.isNetworkAvailable(context));
            }
        }
    };

    @SuppressLint("CheckResult")
    private void setUpInputObservables() {
        // when the user is signing in they shouldn;t be able to signin without the email and password being valid
        // use built in regular expressions to check the validity of both and depending on that enable the login button
        Observable<Boolean> emailObservable = RxTextView.textChanges(Objects.requireNonNull(mLoginEmailTIL.getEditText()))
                .skipInitialValue()
                .map(inputText -> !TextUtils.isEmpty(inputText) && Patterns.EMAIL_ADDRESS.matcher(inputText.toString()).matches())
                .distinctUntilChanged();

        Observable<Boolean> passwordObservable = RxTextView.textChanges(Objects.requireNonNull(mLoginPasswordTIL.getEditText()))
                .skipInitialValue()
                .map(inputText -> !TextUtils.isEmpty(inputText) && inputText.length() >= 5)
                .distinctUntilChanged();

        emailObservable.subscribe(isValid -> setLoginEmailInvalidError(!isValid));
        passwordObservable.subscribe(isValid -> setLoginPasswordInvalidError(!isValid));

        Observable.combineLatest(
                emailObservable,
                passwordObservable,
                (emailValid, passwordValid) -> emailValid && passwordValid)
                .distinctUntilChanged()
                .subscribe(valid -> mLoginBTN.setEnabled(valid));
    }

    private void setLoginEmailInvalidError(boolean enabled) {
        mLoginEmailTIL.setErrorEnabled(enabled);
        if (enabled) {
            mLoginEmailTIL.setError(getString(R.string.error_invalid_email));
        }
    }

    private void setLoginPasswordInvalidError(boolean enabled) {
        mLoginPasswordTIL.setErrorEnabled(enabled);
        if (enabled) {
            mLoginPasswordTIL.setError(getString(R.string.error_invalid_password));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        menu.findItem(R.id.action_sign_out).setVisible(currentUser != null);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_sign_out) {
            // sign user out of firebase and reset UI
            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
            if (currentUser != null) {
                FirebaseAuth.getInstance().signOut();
                updateUi(null);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_RESULT_REGISTER) {
            if (resultCode == Activity.RESULT_OK) {
                // update user UI when user has registered an account and returned to this activity
                updateUi(FirebaseAuth.getInstance().getCurrentUser());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                startActivityForResult(new Intent(HomeActivity.this, RegisterActivity.class), ACTIVITY_RESULT_REGISTER);
                overridePendingTransition(R.anim.activity_enter_from_right, R.anim.do_not_move);
                break;
            case R.id.btn_stream:
                String streamIP = mPiIPValueTV.getText().toString();
                closeKeyBoard();
                startStreamActivity(streamIP);
                break;
            case R.id.btn_login:
                // get username and password
                String email = Objects.requireNonNull(mLoginEmailTIL.getEditText()).getText().toString();
                String password = Objects.requireNonNull(mLoginPasswordTIL.getEditText()).getText().toString();
                closeKeyBoard();
                // sign in with firebase
                mPresenter.signIn(email, password);
                break;
        }
    }

    @Override
    public void onRecyclerItemClick(int position) {
        // callback of listener - when list item is clicked start the ImageViewerActivity to view the image and details
        if (mAdapter != null) {
            ImageCapture capture = mAdapter.getItem(position);
            if (capture != null) {
                Intent intent = new Intent(HomeActivity.this, ImageViewerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(ImageViewerActivity.EXTRA_PARCELABLE_CAPTURE, capture);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_enter_from_right, R.anim.do_not_move);
            }
        }
    }

    @Override
    public void onDeleteItemClick(int position) {
        // callback of listener - when list items delete button is clicked delete the item
        if (mAdapter != null) {
            ImageCapture capture = mAdapter.getItem(position);
            mPresenter.deleteImageCapture(capture);
        }
    }

    @Override
    public void updateUi(FirebaseUser user) {
        // invalidate options menu to hide/show the signout button in the menu
        invalidateOptionsMenu();
        // set views visibility depending on user state
        if (user == null) {
            mLoginCV.setVisibility(View.VISIBLE);
            mPieInfoCV.setVisibility(View.GONE);
            mCapturesCard.setVisibility(View.GONE);
            mSensorDataLL.setVisibility(View.GONE);
        } else {
            mPresenter.updateFirebaseDbUser(user, PrefHelper.getRegistrationtoken(getApplicationContext()));
            mLoginCV.setVisibility(View.GONE);
            mPieInfoCV.setVisibility(View.VISIBLE);
            mCapturesCard.setVisibility(View.VISIBLE);
            mSensorDataLL.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoginFailed(@NonNull String error) {
        mLoginPB.setVisibility(View.GONE);
        mLoginProgressTV.setText(error);
        mLoginProgressTV.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.error));
        mLoginProgressTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void toggleLoadingUi(boolean show) {
        if (show) {
            mLoginProgressTV.setText(R.string.progress_signing_in);
            mLoginProgressTV.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.default_text_color));
            mLoginPB.setVisibility(View.VISIBLE);
            mLoginProgressTV.setVisibility(View.VISIBLE);
        } else {
            mLoginPB.setVisibility(View.GONE);
            mLoginProgressTV.setVisibility(View.GONE);
        }
    }

    @Override
    public void manageImageCaptures(RxFirebaseChildEvent<DataSnapshot> captures) {
        // used for reloading the events of the listener attached to the users images for the adapter
        // changes in the captures will refelect in the adapter
        if (mCapturedImagesRV.getVisibility() != View.VISIBLE) {
            mCapturedImagesRV.setVisibility(View.VISIBLE);
        }
        if (mAdapter == null) {
            mAdapter = new ImageCaptureAdapter(this);
            mCapturedImagesRV.setAdapter(mAdapter);
        }
        mAdapter.manageChildItem(captures);
        // depending on item count decide which view to show/hide
        if (mAdapter.getItemCount() == 0) {
            mCapturedImagesRV.setVisibility(View.GONE);
            mEmptyImagesTV.setVisibility(View.VISIBLE);
        } else {
            if (mEmptyImagesTV.getVisibility() == View.VISIBLE) {
                mEmptyImagesTV.setVisibility(View.GONE);
            }
            if (mCapturedImagesRV.getVisibility() == View.GONE) {
                mCapturedImagesRV.setVisibility(View.VISIBLE);
            }
        }
    }

    private void startStreamActivity(@NonNull String streamIP) {
        // start stream activity with the Pi's ip address
        Intent intent = new Intent(HomeActivity.this, StreamActivity.class);
        intent.putExtra(StreamActivity.URL_EXTRA, streamIP);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_enter_from_right, R.anim.do_not_move);
    }

    @Override
    public void setTemperature(@NonNull String temperature) {
        // if text is empty set current value as 0 otherwise parse the current temp string to get just the temp float value
        String currentTemp = TextUtils.isEmpty(mTemperatureValueTV.getText()) ? "0" : mTemperatureValueTV.getText().toString().replace((char) 0x00B0 + "c", "");
        setSensorValueDrawable(currentTemp, temperature, mTemperatureValueTV);
        mTemperatureValueTV.setText(getString(R.string.format_temperature, temperature));

    }

    @Override
    public void setHumidity(@NonNull String humidity) {
        // if text is empty set current value as 0 otherwise parse the current humidity string to get just the humidity float value
        String currentHum = TextUtils.isEmpty(mHumidityValueTV.getText()) ? "0" : mHumidityValueTV.getText().toString().replace("%", "");
        setSensorValueDrawable(currentHum, humidity, mHumidityValueTV);
        mHumidityValueTV.setText(getString(R.string.format_humidity, humidity));
    }

    private void setSensorValueDrawable(@NonNull String oldValue, @NonNull String newValue, @NonNull TextView textView) {
        float current = Float.parseFloat(newValue);
        float previous = Float.parseFloat(oldValue);
        if (previous != 0f) {
            if (current == previous) {
                // Set icon depending to no change as sensor value is the same
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null,
                        ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_sensor_no_change), null);
            } else {
                // Set icon depending on - if current value is greater then previous value
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getApplicationContext(),
                        current > previous ? R.drawable.ic_sensor_rise : R.drawable.ic_sensor_fall), null);
            }
        }
    }

    @Override
    public void setPiIPAddress(@NonNull String piIP) {
        mPiIPValueTV.setText(piIP);
    }

    @Override
    public void setBuzzerState(boolean enabled) {
        mBuzzerSwitch.setChecked(enabled);
    }

    @Override
    public void setPiStatus(boolean online) {
        // if status is online make sure an internet connection is available
        if (online) {
            online = NetworkHelper.isNetworkAvailable(HomeActivity.this);
        }
        mBuzzerSwitch.setEnabled(online);
        mStreamBTN.setEnabled(online);
        mStreamBTN.setTextColor(ContextCompat.getColor(getApplicationContext(), online ? R.color.colorAccent : R.color.disabled_btn));
        mPiIPValueTV.setTextColor(ContextCompat.getColor(getApplicationContext(), online ? R.color.status_online : R.color.status_offline));
    }

    @Override
    public void setPresenter(@NonNull HomeContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unSubscribe();
    }

    private void closeKeyBoard() {
        // used for clocing the keyboard after input has been entered
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
