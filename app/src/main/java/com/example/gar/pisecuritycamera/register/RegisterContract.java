package com.example.gar.pisecuritycamera.register;

import android.support.annotation.NonNull;

import com.example.gar.pisecuritycamera.BasePresenter;
import com.example.gar.pisecuritycamera.BaseView;
import com.google.firebase.auth.FirebaseUser;

/**
 * The contract between {@link RegisterActivity} and {@link RegisterPresenter}
 */
public interface RegisterContract {

    interface View extends BaseView<Presenter> {

        void showLoadingUI(boolean show);

        void registrationSuccess(@NonNull FirebaseUser user, @NonNull String email);

        void registeredUserInDb();

        void showRegisterError(@NonNull String error);

    }


    interface Presenter extends BasePresenter {

        void createUserWitHEmailAndPassword(@NonNull String email, @NonNull String password, @NonNull String passwordConfirmation);

        void createUserDatabaseNode(@NonNull FirebaseUser user, @NonNull String email, @NonNull String token);

    }
}
