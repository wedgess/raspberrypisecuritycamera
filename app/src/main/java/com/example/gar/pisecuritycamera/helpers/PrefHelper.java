package com.example.gar.pisecuritycamera.helpers;

import android.content.Context;
import android.preference.PreferenceManager;

public class PrefHelper {

    private static final String PREF_KEY_REG_ID = "PREF_KEY_REG_ID";

    private PrefHelper() {
        // non-instantiable
    }

    public static void saveRegistrationToken(String token, Context context) {
        setStringPref(PREF_KEY_REG_ID, token, context);
    }

    public static String getRegistrationtoken(Context context) {
        return getStringPref(PREF_KEY_REG_ID, "", context);
    }

    private static String getStringPref(String name, String def, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(name, def);
    }

    private static void setStringPref(String name, String value, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(name, value).apply();
    }

}
