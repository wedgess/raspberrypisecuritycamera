package com.example.gar.pisecuritycamera.register;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.gar.pisecuritycamera.model.FirebaseModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Presenter class for {@link RegisterActivity}
 */
public class RegisterPresenter implements RegisterContract.Presenter {

    @SuppressWarnings("unused")
    private static final String TAG = RegisterPresenter.class.getSimpleName();

    private final RegisterContract.View mRegisterView;
    private final CompositeDisposable mCompositeDesposable;

    RegisterPresenter(RegisterContract.View view) {
        this.mRegisterView = view;
        mRegisterView.setPresenter(this);
        mCompositeDesposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        // do nothing on subscribe
    }

    @Override
    public void unSubscribe() {
        mCompositeDesposable.clear();
    }

    @Override
    public void createUserWitHEmailAndPassword(@NonNull String email, @NonNull String password, @NonNull String passwordConfirmation) {
        mRegisterView.showLoadingUI(true);
        // create user account for firebase
        mCompositeDesposable.add(FirebaseModel.getInstance().createUserWitHEmailAndPassword(FirebaseAuth.getInstance(), email, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(authResult -> {
                    FirebaseUser currentUser = authResult.getUser();
                    if (currentUser != null) {
                        mRegisterView.registrationSuccess(currentUser, email);
                    }
                }, error -> mRegisterView.showRegisterError(error.getMessage())));
    }

    @Override
    public void createUserDatabaseNode(@NonNull FirebaseUser user, @NonNull String email, @NonNull String token) {
        // create user node in firebase database
        mCompositeDesposable.add(FirebaseModel.getInstance().createUserDatabaseNode(user, email, token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(mRegisterView::registeredUserInDb,
                        error -> Log.e(TAG, "Error creating user in DB")));
    }
}
