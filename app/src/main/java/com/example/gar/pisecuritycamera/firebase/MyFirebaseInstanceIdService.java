package com.example.gar.pisecuritycamera.firebase;

import android.annotation.SuppressLint;
import android.util.Log;

import com.example.gar.pisecuritycamera.helpers.PrefHelper;
import com.example.gar.pisecuritycamera.model.FirebaseModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Listen to FCM token changes, if the user is signed in update their token in FB database.
 * <p>
 * Created by gar on 07/04/18.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = MyFirebaseInstanceIdService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }


    @SuppressLint("CheckResult")
    private void sendRegistrationToServer(String token) {
        // update token in preferences
        PrefHelper.saveRegistrationToken(token, getApplicationContext());
        // if user is signed in then update the users FCm token in FB DB
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            FirebaseModel.getInstance().updateUserToken(currentUser.getUid(), token)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(() -> Log.i(TAG, "Users FCM token updated successfully."));
        }
    }
}
