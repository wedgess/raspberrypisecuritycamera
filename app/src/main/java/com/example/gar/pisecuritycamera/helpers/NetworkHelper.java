package com.example.gar.pisecuritycamera.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkHelper {

    private static final String TAG = NetworkHelper.class.getSimpleName();

    /**
     * Check if internet connection is available
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        try {
            // check for both mobile and wifi connection
            int[] networkTypes = {ConnectivityManager.TYPE_MOBILE, ConnectivityManager.TYPE_WIFI};
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                for (int networkType : networkTypes) {
                    if (networkType == activeNetwork.getType()) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            // returning false now anyway
            Log.e(TAG, e.getMessage());
        }
        return false;
    }

}
