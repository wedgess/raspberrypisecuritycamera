package com.example.gar.pisecuritycamera.image;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.gar.pisecuritycamera.model.FirebaseModel;
import com.example.gar.pisecuritycamera.model.ImageCapture;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Presenter to interact between {@link ImageViewerActivity} and {@link FirebaseModel}
 */
public class ImageViewerPresenter implements ImageViewerContract.Presenter {

    @SuppressWarnings("unused")
    private static final String TAG = ImageViewerPresenter.class.getSimpleName();

    private final ImageViewerContract.View mImageViewerView;
    private final ImageCapture mImageCapture;
    private final CompositeDisposable mCompositeDesposable;

    ImageViewerPresenter(ImageViewerContract.View view, ImageCapture imageCapture) {
        this.mImageViewerView = view;
        this.mImageCapture = imageCapture;
        mImageViewerView.setPresenter(this);
        mCompositeDesposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        setUI();
    }

    private void setUI() {
        // set the UI for the image capture
        if (mImageCapture != null) {
            mImageViewerView.setImageCaptureImage(mImageCapture.getUrl());
            mImageViewerView.setImageCaptureTitle(mImageCapture.getTitle());
            mImageViewerView.setImageCaptureTimestamp(mImageCapture.getTimestamp());
        }
    }

    @Override
    public void unSubscribe() {
        mCompositeDesposable.clear();
    }

    @Override
    public void deleteImageCapture(@NonNull ImageCapture imageCapture) {
        // delete the image capture from firebase
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            mCompositeDesposable.add(FirebaseModel.getInstance().deleteImageCapture(imageCapture, currentUser.getUid())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(mImageViewerView::imageDeletedSuccess,
                            error -> Log.d(TAG, "Error deleting image capture")));
        }
    }
}
