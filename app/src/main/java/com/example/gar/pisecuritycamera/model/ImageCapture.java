package com.example.gar.pisecuritycamera.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.Map;

/**
 * This class represents the an image item in the images node for the current user.
 */
@SuppressWarnings("unused")
public class ImageCapture implements Parcelable {

    private String id;
    private String url;
    private String timestamp;
    private String title;
    private String path;

    public ImageCapture() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.url);
        dest.writeString(this.timestamp);
        dest.writeString(this.title);
        dest.writeString(this.path);
    }

    protected ImageCapture(Parcel in) {
        this.id = in.readString();
        this.url = in.readString();
        this.timestamp = in.readString();
        this.title = in.readString();
        this.path = in.readString();
    }

    public static final Parcelable.Creator<ImageCapture> CREATOR = new Parcelable.Creator<ImageCapture>() {
        @Override
        public ImageCapture createFromParcel(Parcel source) {
            return new ImageCapture(source);
        }

        @Override
        public ImageCapture[] newArray(int size) {
            return new ImageCapture[size];
        }
    };

    @Override
    public String toString() {
        return "ImageCapture{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", title='" + title + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    public static ImageCapture parseData(Map<String, String> data) {
        Gson gson = new Gson();
        JsonElement jsonElement = gson.toJsonTree(data);
        return gson.fromJson(jsonElement, ImageCapture.class);
    }
}
