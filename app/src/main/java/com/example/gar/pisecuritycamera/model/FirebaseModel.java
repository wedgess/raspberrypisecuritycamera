package com.example.gar.pisecuritycamera.model;

import android.support.annotation.NonNull;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import durdinapps.rxfirebase2.RxFirebaseAuth;
import durdinapps.rxfirebase2.RxFirebaseChildEvent;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import durdinapps.rxfirebase2.RxFirebaseStorage;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;

/**
 * Class that handles all the firebase interactions for the presenters.
 */
public class FirebaseModel {

    @SuppressWarnings("unused")
    private static final String TAG = FirebaseModel.class.getSimpleName();

    // nodes used in Firebase Database
    private static final String USERS_ROOT_NODE = "users";
    private static final String USERS_EMAIL_NODE = "email";
    private static final String USERS_TOKEN_NODE = "token";
    private static final String USERS_IMAGES_NODE = "images";
    private static final String USERS_BUZZER_NODE = "buzzer";
    private static final String USERS_PI_STATUS_NODE = "online";
    private static final String USERS_SENSOR_DATA_NODE = "sensors";
    private static final String USERS_PI_IP_DATA_NODE = "pi_ip";

    private static volatile FirebaseModel sSoleInstance;

    public static FirebaseModel getInstance() {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time

            synchronized (FirebaseModel.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null)
                    sSoleInstance = new FirebaseModel();
            }
        }

        return sSoleInstance;
    }

    private FirebaseModel() {
        //Non instantiable
    }

    public Maybe<Boolean> signIn(@NonNull String email, @NonNull String password, @NonNull FirebaseAuth auth) {
        // sign user in with email and password
        return RxFirebaseAuth.signInWithEmailAndPassword(auth, email, password)
                .map(authResult -> authResult.getUser() != null);
    }

    public Flowable<RxFirebaseChildEvent<DataSnapshot>> listenToImageCaptureEvents(@NonNull String userId) {
        // Get db ref to users images and set listener
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersImagesRef = database.getReference(FirebaseModel.USERS_ROOT_NODE).child(userId).child(USERS_IMAGES_NODE);
        return RxFirebaseDatabase.observeChildEvent(usersImagesRef);
    }

    public Observable<FirebaseAuth> listenToAuthState() {
        // listener for user authentication state
        return RxFirebaseAuth.observeAuthState(FirebaseAuth.getInstance());
    }

    public Completable updateUserToken(@NonNull String userId, @NonNull String registrationToken) {
        // Update users FCM token in FB DB
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userTokenRef = database.getReference(FirebaseModel.USERS_ROOT_NODE).child(userId).child(FirebaseModel.USERS_TOKEN_NODE);
        return RxFirebaseDatabase.setValue(userTokenRef, registrationToken);
    }

    public Completable enableBuzzer(@NonNull String userId, boolean enabled) {
        // Update users buzzer value in FB DB
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userBuzzerRef = database.getReference(FirebaseModel.USERS_ROOT_NODE).child(userId).child(FirebaseModel.USERS_BUZZER_NODE);
        return RxFirebaseDatabase.setValue(userBuzzerRef, enabled);
    }

    public Completable deleteImageCapture(@NonNull ImageCapture capture, @NonNull String userId) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference imageRef = database.getReference().child(USERS_ROOT_NODE).child(userId).child(USERS_IMAGES_NODE).child(capture.getId());
        // delete image from the users images tree
        Completable deleteImageNodeCompletable = Completable.fromAction(imageRef::removeValue);
        // delete the image from FB storag
        StorageReference photoRef = FirebaseStorage.getInstance().getReferenceFromUrl(capture.getUrl());
        // join deleting the image node and deleting the image from storage
        return deleteImageNodeCompletable.andThen(RxFirebaseStorage.delete(photoRef));
    }

    public Maybe<AuthResult> createUserWitHEmailAndPassword(@NonNull FirebaseAuth auth, @NonNull String email, @NonNull String password) {
        // register a user with firebase using email and password
        return RxFirebaseAuth.createUserWithEmailAndPassword(auth, email, password);
    }

    public Completable createUserDatabaseNode(@NonNull FirebaseUser currentUser, @NonNull String email, @NonNull String token) {
        // create the user in FB DB
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference(FirebaseModel.USERS_ROOT_NODE);
        Completable createUserEmailCompletable = RxFirebaseDatabase.setValue(usersRef.child(currentUser.getUid()).child(USERS_EMAIL_NODE), email);
        Completable createUserTokenCompletable = RxFirebaseDatabase.setValue(usersRef.child(currentUser.getUid()).child(USERS_TOKEN_NODE), token);
        // join creating token and email
        return createUserEmailCompletable.andThen(createUserTokenCompletable);
    }

    public Flowable<SensorData> listenToSensorDataEvents(@NonNull String userId) {
        // Gte DB ref to sensors node and listen to value changes
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersImagesRef = database.getReference(FirebaseModel.USERS_ROOT_NODE).child(userId).child(USERS_SENSOR_DATA_NODE);
        return RxFirebaseDatabase.observeValueEvent(usersImagesRef, SensorData.class);
    }

    public Flowable<Boolean> listenToPiStatusEvents(@NonNull String userId) {
        // Gte DB ref to sensors node and listen to value changes
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersPiStatusRef = database.getReference(FirebaseModel.USERS_ROOT_NODE).child(userId).child(USERS_PI_STATUS_NODE);
        return RxFirebaseDatabase.observeValueEvent(usersPiStatusRef, Boolean.class);
    }

    public Maybe<Boolean> getPiStatus(@NonNull String userId) {
        // Gte DB ref to sensors node and listen to value changes
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersPiStatusRef = database.getReference(FirebaseModel.USERS_ROOT_NODE).child(userId).child(USERS_PI_STATUS_NODE);
        return RxFirebaseDatabase.observeSingleValueEvent(usersPiStatusRef, Boolean.class);
    }

    public Flowable<String> listenToPiIPDataEvents(@NonNull String userId) {
        // Get DB ref to pi IP node and listen to changes
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersPiIPRef = database.getReference(FirebaseModel.USERS_ROOT_NODE).child(userId).child(USERS_PI_IP_DATA_NODE);
        return RxFirebaseDatabase.observeValueEvent(usersPiIPRef, String.class);
    }

    public Flowable<Boolean> listenToBuzzerEvents(@NonNull String userId) {
        // Gte DB ref to buzzer node and listen to value changes
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersBuzzerRef = database.getReference(FirebaseModel.USERS_ROOT_NODE).child(userId).child(USERS_BUZZER_NODE);
        return RxFirebaseDatabase.observeValueEvent(usersBuzzerRef, Boolean.class);
    }
}
