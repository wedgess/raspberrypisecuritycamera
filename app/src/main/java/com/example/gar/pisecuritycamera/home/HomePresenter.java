package com.example.gar.pisecuritycamera.home;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.gar.pisecuritycamera.model.FirebaseModel;
import com.example.gar.pisecuritycamera.model.ImageCapture;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Presenter for {@link HomeActivity}, model is {@link FirebaseModel}
 */
public class HomePresenter implements HomeContract.Presenter {

    private static final String TAG = HomePresenter.class.getSimpleName();

    private final FirebaseAuth mAuth;
    private final HomeContract.View mHomeView;
    private final CompositeDisposable mCompositeDesposable;

    HomePresenter(HomeContract.View homeView) {
        this.mAuth = FirebaseAuth.getInstance();
        this.mHomeView = homeView;
        this.mHomeView.setPresenter(this);
        mCompositeDesposable = new CompositeDisposable();
    }

    @Override
    public void updateFirebaseDbUser(@NonNull FirebaseUser currentUser, @NonNull String regToken) {
        // create the user in FB DB
        mCompositeDesposable.add(FirebaseModel.getInstance().updateUserToken(currentUser.getUid(), regToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(() -> Log.i(TAG, "Users FCM token updated successfully.")));

    }

    @Override
    public void signIn(@NonNull String email, @NonNull String password) {
        mHomeView.toggleLoadingUi(true);
        // check if username and password are empty, otherwise attempy to sign in
        mCompositeDesposable.add(FirebaseModel.getInstance().signIn(email, password, mAuth)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(success -> {
                    mHomeView.toggleLoadingUi(false);
                    if (success) {
                        FirebaseUser user = mAuth.getCurrentUser();
                        mHomeView.updateUi(user);
                    }
                }, error -> mHomeView.showLoginFailed(error.getMessage())));
    }

    @Override
    public void addImageCaptureListener(@NonNull FirebaseUser user) {
        // listen to events on users image captures FB node
        mCompositeDesposable.add(FirebaseModel.getInstance().listenToImageCaptureEvents(user.getUid())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(mHomeView::manageImageCaptures,
                        error -> Log.e(TAG, "Error occurred trying to listen to users images: " + error.getMessage())));
    }

    @Override
    public void addSensorDataListener(@NonNull FirebaseUser user) {
        // listen to events on users sensors FB node
        mCompositeDesposable.add(FirebaseModel.getInstance().listenToSensorDataEvents(user.getUid())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(sensorData -> {
                    mHomeView.setTemperature(sensorData.getTemperature());
                    mHomeView.setHumidity(sensorData.getHumidity());
                    Log.i(TAG, "addSensorDataListener: " + sensorData.toString());
                }, error -> Log.e(TAG, "Error occurred trying to listen to sensor data: " + error.getMessage())));
    }

    @Override
    public void addPiIPListener(@NonNull FirebaseUser user) {
        // listen to events on users PI IP FB node
        mCompositeDesposable.add(FirebaseModel.getInstance().listenToPiIPDataEvents(user.getUid())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(mHomeView::setPiIPAddress,
                        error -> Log.e(TAG, "Error occurred trying to listen to sensor data: " + error.getMessage())));
    }

    @Override
    public void addBuzzerListener(@NonNull FirebaseUser user) {
        // listen to events on users PI IP FB node
        mCompositeDesposable.add(FirebaseModel.getInstance().listenToBuzzerEvents(user.getUid())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(mHomeView::setBuzzerState,
                        error -> Log.e(TAG, "Error occurred trying to listen to buzzer state: " + error.getMessage())));
    }

    @Override
    public void enableBuzzer(boolean enabled) {
        mCompositeDesposable.add(FirebaseModel.getInstance()
                .enableBuzzer(mAuth.getCurrentUser().getUid(), enabled)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(() -> Log.i(TAG, "Buzzer value updated")));
    }

    @Override
    public void deleteImageCapture(@NonNull ImageCapture capture) {
        // delete a specific image capture from firebase
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            mCompositeDesposable.add(FirebaseModel.getInstance().deleteImageCapture(capture, currentUser.getUid())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(() -> Log.i(TAG, "Image capture deleted")));
        }
    }

    @Override
    public void addPiStatusListener(@NonNull FirebaseUser user) {
        // listen to events on users PI IP FB node
        mCompositeDesposable.add(FirebaseModel.getInstance().listenToPiStatusEvents(user.getUid())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(mHomeView::setPiStatus,
                        error -> Log.e(TAG, "Error occurred trying to listen to buzzer state: " + error.getMessage())));
    }

    @Override
    public void setPiStatusBasedOnConnection(@NonNull FirebaseUser user, boolean hasInternet) {
        mCompositeDesposable.add(FirebaseModel.getInstance().getPiStatus(user.getUid())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(online -> {
                    if (online && hasInternet) {
                        mHomeView.setPiStatus(true);
                    } else {
                        mHomeView.setPiStatus(false);
                    }
                }));
    }

    private void listenToAuthState() {
        // listener for when user is signed in or out
        mCompositeDesposable.add(FirebaseModel.getInstance().listenToAuthState()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(firebaseAuth -> {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        // User is signed in so set all listeners to user nodes
                        Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                        mHomeView.updateUi(user);
                        addImageCaptureListener(user);
                        addSensorDataListener(user);
                        addPiIPListener(user);
                        addPiStatusListener(user);
                        addBuzzerListener(user);
                    } else {
                        // User is signed out update user UI
                        Log.d(TAG, "onAuthStateChanged:signed_out");
                        mHomeView.updateUi(null);
                    }
                }));
    }

    @Override
    public void subscribe() {
        listenToAuthState();
    }

    @Override
    public void unSubscribe() {
        mCompositeDesposable.clear();
    }
}
