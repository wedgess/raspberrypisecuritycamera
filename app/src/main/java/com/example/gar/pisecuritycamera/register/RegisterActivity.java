package com.example.gar.pisecuritycamera.register;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.gar.pisecuritycamera.R;
import com.example.gar.pisecuritycamera.helpers.PrefHelper;
import com.google.firebase.auth.FirebaseUser;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

/**
 * Activity for the user to register an account.
 */
public class RegisterActivity extends AppCompatActivity implements RegisterContract.View, View.OnClickListener {

    @SuppressWarnings("unused")
    private static final String TAG = RegisterActivity.class.getSimpleName();

    //region View binding
    @BindView(R.id.til_register_email)
    TextInputLayout mRegisterEmailTIL;
    @BindView(R.id.til_register_password)
    TextInputLayout mRegisterPasswordTIL;
    @BindView(R.id.til_register_password_confirmation)
    TextInputLayout mRegisterPasswordConfirmationTIL;
    @BindView(R.id.btn_register_account)
    Button mRegisterBTN;
    @BindView(R.id.pb_register)
    ProgressBar mRegisterPB;
    @BindView(R.id.tv_register_progress)
    TextView mRegisterProgressTV;
    //endregion

    private RegisterContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set-up view, view binding and activity actionbar + presenter
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        new RegisterPresenter(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        setUpInputObservables();

        mRegisterBTN.setOnClickListener(this);
    }

    @SuppressLint("CheckResult")
    private void setUpInputObservables() {
        // when the user is registering they shouldn;t be able to register without the email, password  & confirmation being valid
        // use built in regular expressions to check the validity of inputs and depending on that enable the register button
        Observable<Boolean> emailObservable = RxTextView.textChanges(Objects.requireNonNull(mRegisterEmailTIL.getEditText()))
                .skipInitialValue()
                .map(inputText -> !TextUtils.isEmpty(inputText) && Patterns.EMAIL_ADDRESS.matcher(inputText.toString()).matches())
                .distinctUntilChanged();


        Observable<Boolean> passwordObservable = RxTextView.textChanges(Objects.requireNonNull(mRegisterPasswordTIL.getEditText()))
                .skipInitialValue()
                .map(inputText -> !TextUtils.isEmpty(inputText) && inputText.length() > 5)
                .distinctUntilChanged();

        Observable<Boolean> passwordConfirmationObservable = RxTextView.textChanges(Objects.requireNonNull(mRegisterPasswordConfirmationTIL.getEditText()))
                .skipInitialValue()
                .map(inputText -> !TextUtils.isEmpty(inputText) && inputText.toString().equals(mRegisterPasswordTIL.getEditText().getText().toString()))
                .distinctUntilChanged();

        // subscribe to changes on each observable
        emailObservable.subscribe(isValid -> setRegisterEmailInvalidError(!isValid));
        passwordObservable.subscribe(isValid -> setRegisterPasswordInvalidError(!isValid));
        passwordConfirmationObservable.subscribe(isValid -> setRegisterPasswordConfirmationInvalidError(!isValid));

        // combine all observables and is all are valid enable the register button
        Observable.combineLatest(
                emailObservable,
                passwordObservable,
                passwordConfirmationObservable,
                (emailValid, passwordValid, passwordConformationValid) -> emailValid && passwordValid && passwordConformationValid)
                .distinctUntilChanged()
                .subscribe(valid -> mRegisterBTN.setEnabled(valid));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setRegisterEmailInvalidError(boolean enabled) {
        mRegisterEmailTIL.setErrorEnabled(enabled);
        if (enabled) {
            mRegisterEmailTIL.setError(getString(R.string.error_invalid_email));
        }
    }

    private void setRegisterPasswordInvalidError(boolean enabled) {
        mRegisterPasswordTIL.setErrorEnabled(enabled);
        if (enabled) {
            mRegisterPasswordTIL.setError(getString(R.string.error_invalid_password));
        }
    }

    private void setRegisterPasswordConfirmationInvalidError(boolean enabled) {
        mRegisterPasswordConfirmationTIL.setErrorEnabled(enabled);
        if (enabled) {
            mRegisterPasswordConfirmationTIL.setError(getString(R.string.error_confirmation_password));
        }
    }

    @Override
    public void setPresenter(@NonNull RegisterContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.subscribe();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void showLoadingUI(boolean show) {
        if (show) {
            mRegisterProgressTV.setText(R.string.progress_register);
            mRegisterProgressTV.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.default_text_color));
            mRegisterPB.setVisibility(View.VISIBLE);
            mRegisterProgressTV.setVisibility(View.VISIBLE);
        } else {
            mRegisterPB.setVisibility(View.GONE);
            mRegisterProgressTV.setVisibility(View.GONE);
        }
    }

    @Override
    public void registrationSuccess(@NonNull FirebaseUser user, @NonNull String email) {
        mPresenter.createUserDatabaseNode(user, email, PrefHelper.getRegistrationtoken(getApplicationContext()));
    }

    @Override
    public void registeredUserInDb() {
        showLoadingUI(false);

        // finish activity and return to Home
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void showRegisterError(@NonNull String error) {
        mRegisterPB.setVisibility(View.GONE);
        mRegisterProgressTV.setText(error);
        mRegisterProgressTV.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.error));
        mRegisterProgressTV.setVisibility(View.VISIBLE);
    }

    private void closeKeyBoard() {
        // close keyboard when user input is complete and register button is pressed
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(inputManager).hideSoftInputFromWindow((null == getCurrentFocus()) ? null
                : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_register_account) {
            // get username and password and create firebase account with them
            String email = Objects.requireNonNull(mRegisterEmailTIL.getEditText()).getText().toString();
            String password = Objects.requireNonNull(mRegisterPasswordTIL.getEditText()).getText().toString();
            String passwordConfirmation = Objects.requireNonNull(mRegisterPasswordConfirmationTIL.getEditText()).getText().toString();

            closeKeyBoard();
            mPresenter.createUserWitHEmailAndPassword(email, password, passwordConfirmation);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.do_not_move, R.anim.activity_exit_from_right);
    }
}
