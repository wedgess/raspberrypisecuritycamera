package com.example.gar.pisecuritycamera;

import android.support.annotation.NonNull;

/**
 * Base view which all applications view classes implement
 */
public interface BaseView<T> {

    void setPresenter(@NonNull T presenter);

}
