package com.example.gar.pisecuritycamera.image;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.gar.pisecuritycamera.R;
import com.example.gar.pisecuritycamera.model.ImageCapture;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Basic activity to display an {@link ImageCapture}. Activity is used for displaying the notifications and Recycler items.
 */
public class ImageViewerActivity extends AppCompatActivity implements ImageViewerContract.View {

    @SuppressWarnings("unused")
    private static final String TAG = ImageViewerActivity.class.getSimpleName();
    public static final String EXTRA_PARCELABLE_CAPTURE = "EXTRA_PARCELABLE_CAPTURE";

    //region View binding
    @BindView(R.id.toolbar_image_activity)
    Toolbar toolbar;
    @BindView(R.id.imageView)
    PhotoView mImageView;
    @BindView(R.id.tv_img_title_value)
    TextView mImageTitleTV;
    @BindView(R.id.tv_img_timestamp_value)
    TextView mImageTimestampTV;
    //endregion

    private ImageViewerContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set-up view, view binding and activity actionbar + presenter
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        new ImageViewerPresenter(this, getIntent().getParcelableExtra(EXTRA_PARCELABLE_CAPTURE));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_image, menu);
        return true;
    }

    private void saveImageToGallery() {
        // get the bitmap from the ImageView and save the bitmap to the media gallery
        mImageView.setDrawingCacheEnabled(true);
        Bitmap bitmap = mImageView.getDrawingCache();
        ImageCapture capture = getIntent().getParcelableExtra(EXTRA_PARCELABLE_CAPTURE);
        MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, capture.getTitle(), capture.getTimestamp());
        Snackbar.make(mImageView.getRootView(), R.string.snackbar_msg_image_saved, 3000).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_delete_image) {
            // if delete get the image capture from the intent and then delete
            ImageCapture capture = getIntent().getParcelableExtra(EXTRA_PARCELABLE_CAPTURE);
            mPresenter.deleteImageCapture(capture);
            return true;
        } else if (item.getItemId() == R.id.action_save_image) {
            saveImageToGallery();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(@NonNull ImageViewerContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setImageCaptureImage(@NonNull String imgUrl) {
        Glide.with(this)
                .load(imgUrl)
                .into(mImageView);
    }

    @Override
    public void setImageCaptureTitle(@NonNull String title) {
        mImageTitleTV.setText(title);
    }

    @Override
    public void setImageCaptureTimestamp(@NonNull String timestamp) {
        mImageTimestampTV.setText(timestamp);
    }

    @Override
    public void imageDeletedSuccess() {
        Toast.makeText(getApplicationContext(), R.string.toast_msg_image_deleted, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.do_not_move, R.anim.activity_exit_from_right);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.subscribe();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }
}
