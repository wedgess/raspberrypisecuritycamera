package com.example.gar.pisecuritycamera.model;

/**
 * This class represents the users sensor data stored in sensors node for the current user.
 */

@SuppressWarnings("unused")
public class SensorData {

    private String temperature;
    private String humidity;

    public SensorData() {
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "SensorData{" +
                "temperature='" + temperature + '\'' +
                ", humidity='" + humidity + '\'' +
                '}';
    }
}
