package com.example.gar.pisecuritycamera.image;

import android.support.annotation.NonNull;

import com.example.gar.pisecuritycamera.BasePresenter;
import com.example.gar.pisecuritycamera.BaseView;
import com.example.gar.pisecuritycamera.model.ImageCapture;

/**
 * Contract between {@link ImageViewerActivity} and {@link ImageViewerPresenter}
 */
public interface ImageViewerContract {

    interface View extends BaseView<Presenter> {

        void setImageCaptureImage(@NonNull String imgUrl);

        void setImageCaptureTitle(@NonNull String title);

        void setImageCaptureTimestamp(@NonNull String timestamp);

        void imageDeletedSuccess();
    }

    interface Presenter extends BasePresenter {

        void deleteImageCapture(@NonNull ImageCapture imageCapture);

    }
}
