package com.example.gar.pisecuritycamera.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import com.example.gar.pisecuritycamera.R;
import com.example.gar.pisecuritycamera.image.ImageViewerActivity;
import com.example.gar.pisecuritycamera.model.ImageCapture;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

/**
 * Handles FCM notifications.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    public MyFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // ...
        Log.i(TAG, "Message received");

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            // parse data
            ImageCapture capture = ImageCapture.parseData(remoteMessage.getData());

            // sent intent so ImageViewerActivity is started on notification click
            Intent intent = new Intent(this, ImageViewerActivity.class);
            intent.putExtra(ImageViewerActivity.EXTRA_PARCELABLE_CAPTURE, capture);
            final PendingIntent pendingIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            final NotificationManager notificationManager =
                    (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            // show only most recent notification of the group
            final int notifyID = 1;
            // if Android O or greater set up notification channel
            String CHANNEL_ID = getString(R.string.channel_id);// The id of the channel.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence name = getPackageName();// The user-visible name of the channel.
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{200, 300, 200, 400});
                Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);
            }

            // build notification
            Notification.Builder builder = buildNotification(getBitmapFromUrl(capture.getUrl()), capture.getTitle(), capture.getTimestamp(),
                    pendingIntent, CHANNEL_ID, getApplicationContext());
            Objects.requireNonNull(notificationManager).notify(notifyID, builder.build());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    // Build message notification
    public static Notification.Builder buildNotification(Bitmap img, String title, String content, PendingIntent pendingIntent, String channelId, Context appContext) {
        Notification.Builder builder = new Notification.Builder(appContext)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.drawable.ic_warning_notif)
                .setLargeIcon(img)
                .setStyle(new Notification.BigPictureStyle()
                        .bigPicture(img))/*Notification with Image*/
                .setLights(Color.RED, 300, 100)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setVibrate(new long[]{0, 50, 1000})
                .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(channelId);
        }

        return builder;
    }

    /*
     *To get a Bitmap image from the URL received
     * */
    public Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }
}
